typedef struct
{
    int *liste_sommet;
    int poids;
}etat;

int calculPoids(int** grapheComplet, etat* state, int taille);
etat *genereCycleHamiltonien(int **grapheComplet,int taille);
int* permuter(int* liste, int i, int j);
void genereEtat(int** grapheComplet, etat * state, etat * newState, int taille);

