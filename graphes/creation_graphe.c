#include"../parcours/floyd_warshall.h"
#include "./creation_graphe.h"

int **genere(int **matrice,int taille , int bas, int haut)
{


    if (bas < haut)
    {
        int k = rand() % (haut - bas) + bas;
        matrice[bas][bas + 1] = 1;
        matrice[bas + 1][bas] = 1;
        if (k + 1 <= haut)
        {
            matrice[bas][k + 1] = 1;
            matrice[k + 1][bas] = 1;
        }
        genere(matrice,taille,bas + 1, k);
        genere(matrice,taille,k + 1, haut);
    }
    return matrice;
}


int **genereGraphe(int **matrice,int taille,float p)
{

    printf("GENERE Graphe N : %d\n", taille);
    p=p*100 ; // pourcentage
    for (int i = 0; i < taille; i++)
    {
        for (int j = i + 1; j < taille; j++)
        {
            if (rand() % 100 < p)
            {
                matrice[i][j] = 1;
                matrice[j][i] = 1;
            }
        }
    }
    return matrice;
}

int **genereGrapheComplet(int **matrice,int taille)
{
    int **distance = floyd_warshall(matrice,taille);
    printf("GENERE Graphe N : %d\n", taille);
    for (int i = 0; i < taille; i++)
    {
        for (int j = i + 1; j < taille; j++)
        {    
            if (distance[i][j] == 0)
            {
                matrice[i][j] = distance[i][j];
                matrice[j][i] = distance[i][j];
            }
        }
    }
    return matrice;
}

void afficheMatrice(int** matrice, int taille)
{
    
    for (int i = 0; i < taille; i++)
    {
        printf("[");
        for (int j = 0; j < taille; j++)
        {
            printf("%d", matrice[i][j]);
            if (j < taille - 1)
            {
                printf(", ");
            }
        }
        printf("]\n");
    }
}

int **creationMatrice(int taille)
{
    int **matrice = malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        matrice[i] = malloc(taille * sizeof(int));
        for (int j = 0; j < taille; j++)
        {
            matrice[i][j] = 0;
        }
    }
    return matrice;
}



void freeMatrice(int **matrice, int taille)
{

    for (int i = 0; i < taille; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
}






