#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int **genere(int **matrice, int taille, int bas, int haut);
int **genereGraphe(int **matrice,int taille,float p);
void afficheMatrice(int ** matrice,int taille);
int **creationMatrice(int taille);
int **genereGrapheComplet(int **matrice,int taille);
void freeMatrice(int **matrice,int taille);

