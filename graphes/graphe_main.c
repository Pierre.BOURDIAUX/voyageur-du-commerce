#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "./creation_graphe.h"


int main(int argc, char const *argv[])
{
    int N = 10;
    int ** matrice = creationMatrice(N);
    srand(time(NULL));
    matrice = genere(matrice, N,0, N - 1);
    afficheMatrice(matrice,N);

    matrice = genereGraphe(matrice,N, 0.5);
    printf("\n");
    afficheMatrice(matrice,N);
    freeMatrice(matrice,N);
    return 0;
}