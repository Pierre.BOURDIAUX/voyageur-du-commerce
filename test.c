#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "recuit_simulé.h"
#include "graphes/creation_graphe.h"
#include"parcours/floyd_warshall.h"

int main()
{
    int N = 10;
    srand(time(NULL));
    int ** matrice = genere(creationMatrice(N),N,0,3);
    int **graphe = genereGraphe(matrice,N,0.5);
    int **grapheComplet =floyd_warshall(graphe,N);
    etat * state = simulatedAnnealing(grapheComplet,N,10000,calculPoids);
    for (int i = 0; i < N+1; i++) printf("%d ",state->liste_sommet[i]);
    printf("\n");
    printf("Poids : %d\n",state->poids);
    free(state);
    freeMatrice(matrice,N);
    freeMatrice(grapheComplet,N);
    return 0;
}