# Variables
CC = gcc 
CFLAGS = -g -Wall -Wextra
LDFLAGS = -lSDL2 -lSDL2_ttf -lm

# Répertoires
RECUIT_DIR = .
GRAPHES_DIR = ./graphes
PARCOURS_DIR = ./parcours

# Fichiers sources
GRAPHES_SOURCES = $(GRAPHES_DIR)/creation_graphe.c

RECUIT_SOURCES = $(RECUIT_DIR)/recuit_simulé.c \
				 $(RECUIT_DIR)/genereEtat.c
				 
PARCOURS_SOURCES = $(PARCOURS_DIR)/floyd_warshall.c 


# Fichiers objets
OBJDIR = .
GRAPHES_OBJECTS = $(GRAPHES_SOURCES:$(GRAPHES_DIR)/%.c=$(OBJDIR)/%.o)
PARCOURS_OBJECTS = $(PARCOURS_SOURCES:$(PARCOURS_DIR)/%.c=$(OBJDIR)/%.o)
RECUIT_OBJECTS = $(RECUIT_SOURCES:$(RECUIT_DIR)/%.c=$(OBJDIR)/%.o)


# Exécutable
EXECUTABLE = affichage

# Règle par défaut
all: $(EXECUTABLE)

# Règle de construction de l'exécutable
$(EXECUTABLE): $(RECUIT_OBJECTS) $(GRAPHES_OBJECTS) $(PARCOURS_OBJECTS)
	$(CC) $^ -o $@ $(LDFLAGS)

# Règle de construction des objets pour le répertoire de ./recuit_simulé.c
$(OBJDIR)/%.o: $(RECUIT_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

# Règle de construction des objets pour le répertoire "graphes/creation_graphe"
$(OBJDIR)/%.o: $(GRAPHES_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

# Règle de construction des objets pour le répertoire "parcours/floyd_warshall"
$(OBJDIR)/%.o: $(PARCOURS_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

# Nettoyage des fichiers objets et de l'exécutable
clean:
	rm -f $(OBJDIR)/*.o $(EXECUTABLE)