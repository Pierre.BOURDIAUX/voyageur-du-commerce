
#include "recuit_simulé.h"
#include "graphes/creation_graphe.h"






etat * simulatedAnnealing(int ** grapheComplet,int taille, int T, int calculPoids(int** grapheComplet, etat *state, int taille)) 
{
    etat * state = genereCycleHamiltonien(grapheComplet, taille);
    etat * newState = malloc(sizeof(etat));
    while (T > 0) 
    {
        genereEtat(grapheComplet, state, newState, taille);
        if (calculPoids(grapheComplet,newState,taille) < calculPoids(grapheComplet,state,taille)) 
        if (calculPoids(grapheComplet,newState,taille) > state->poids) state=newState;
        double p = exp((state->poids - calculPoids(grapheComplet,newState,taille)) / T);
        if (p > rand() / RAND_MAX) state=newState;
        T--;
    }
    return state;
}