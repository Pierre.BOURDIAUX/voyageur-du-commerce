#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <SDL2/SDL.h>

#ifndef __COLONIES_FOURMIS_H__
#define __COLONIES_FOURMIS_H__

#define MAX_ITERATIONS 100  // Nombre maximum d'itérations
#define NUM_ANTS 100          // Nombre de fourmis
#define ALPHA 1.0            // Facteur d'importance de la phéromone
#define BETA 2.0             // Facteur d'importance de l'heuristique
#define RHO 0.5              // Taux d'évaporation de la phéromone
#define Q 100                // Quantité de phéromone déposée par une fourmi

typedef int** Graphe;



// Structure représentant une arête du graphe
typedef struct {
    double pheromone;       // Quantité de phéromone sur l'arête
    double distance;        // Distance entre les noeuds adjacents
} Edge;

Edge ** initialisation_graphe_edge (int ** graphe, int numNodes) ;
// Fonction pour libérer la mémoire allouée pour le graphe
void freeGraph(Edge** graph, int numNodes);

// Fonction pour mettre à jour la phéromone évaporée sur toutes les arêtes
void evaporatePheromone(Edge** graph, int numNodes);

// Fonction pour effectuer le mouvement d'une fourmi
int antMove(Edge** graph, int numNodes, int startNode, int* visited,int numEtape,int Start);

// Fonction principale pour résoudre le problème en utilisant les colonies de fourmis
int antColonyOptimization(Edge** graph, int numNodes, int startNode, int* bestTour);

#endif