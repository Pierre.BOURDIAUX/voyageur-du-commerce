#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include"./algo_colonies_fourmis.h"
#include "../graphes/creation_graphe.h"


int main(int argc, char **argv) 
{
    (void)argc;
    (void)argv;

    int ** matrice_distance = creationMatrice(4);
    for (int i =0;i<4;i++)
    {
        for (int j=0;j<4;j++)
        {
            if (((j=i+1)&&(j<3))||((j=i-1)&&(i>0)))
            {
                matrice_distance [i][j]=2;
            }
            else
            {
                if (i!=j)
                {
                    matrice_distance [i][j]=3;
                }
            }
        }
    }
    afficheMatrice(matrice_distance,4);
    printf("\n Le graphe complet représenter si dessus par sa matrice de distance possède un cout pour un cycle hamiltonien de 8\n");

    antColony(100,0.1,2,0.03,0.5);


    return 0;
}
