#include "floyd_warshall.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Fonction qui prend en entrée un graphe et qui retourne la matrice des distances entre les sommets du graphe
 * 
 * @param graphe  
 * @return int ** distance
 */
int **floyd_warshall(int ** matrice_adjacence_graphe,int taille)
{
    int ** distance = malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        distance[i] = malloc(taille * sizeof(float));
    }
    for (int i = 0; i < taille; i++)
    {
        distance[i][i] = 0.0;
        for (int j = i + 1; j < taille; j++)
        {
            if (matrice_adjacence_graphe[i][j] != 0)
            {
                distance[i][j] = 1;
                distance[j][i] = 1;
            }
            else
            {
                distance[i][j] = 999999;
                distance[j][i] = 999999;
            }
        }
    }
    for (int k = 0; k < taille; k++)
    {
        for (int i = 0; i < taille; i++)
        {   
            for (int j = 0; j < taille; j++)
            {
                if (distance[i][j] > distance[i][k] + distance[k][j])
                {
                    distance[i][j] = distance[i][k] + distance[k][j];
                }
            }
        }
    }
    return distance;
}

