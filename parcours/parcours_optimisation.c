#include "./parcours_optimisation.h"

double factorielle (int n)
{
    int i;
    double retour=1;
    if (n>0)
    {
        for (i=1;i<n;i++)
        {
            retour= retour*i;
        }
    }
    return(retour);
    
}

//Chaque graphe est supposé complet
double calcul_cycle_possible (int nmb_sommets)
{
    double nmb_cycle ;
    if (nmb_sommets%2 ==0)
    {
        nmb_cycle=factorielle(nmb_sommets-1);
    }
    else
    {
        nmb_cycle=factorielle(nmb_sommets);
    }
    return(nmb_cycle);
}

//hypothèse traitement de 10^9 cycles par secondes
void Crea_table_traitement (int * nmb_sommets,int taille_liste)
{
    double nmb_cyclej=0;
    printf("nombre de sommets | nombre de cycles possibles | temps de traitement en secondes \n");
    for (int j = 0; j < taille_liste; j++)
    {
        nmb_cyclej = calcul_cycle_possible(nmb_sommets[j]);
        printf("%d | %E | %E\n",nmb_sommets[j],nmb_cyclej,nmb_cyclej/pow(10,9));
    }

}

//Méthode gloutonne 

//Renvoie 0 si la liste contient la valeur et 1 sinon
int element_present(int *L, int tailleL, int valeur) 
{
    int retour = 1;
    if (tailleL > 0) 
    {
        int i = 0;
        while ((i < tailleL) && (retour != 0)) 
        {
            if (L[i] == valeur) 
            {
                retour=0;
            }
            i++;
        }
    }
    return retour;
}



int cout_glouton (int ** matrice,int nmb_sommets)
{
    int liste [nmb_sommets+10];
    int position=0;
    int taille = 0;
    int cout=0;
    liste[taille]=0;

    while (taille<(nmb_sommets))
    {
        int min;
        int suivant;
        if (position!=0)
        {
            min = matrice[position][0];
            suivant = 0;
        }
        else
        {
            min = matrice[position][1];
            suivant = 1;
        }
        
        for (int j=1;j<nmb_sommets;j++)
        {
            if ((matrice[position][j]<min)&&(position!=j))
            {
                if (element_present(liste,taille,j)!=0)
                {
                    min=matrice[position][j];
                    suivant=j;
                }
            }
        }
        cout= cout + matrice[position][suivant];
        position=suivant;
        taille ++;
        liste[taille]=suivant;
        
    }
    cout = cout + matrice[position][0];
    return (cout);

}



