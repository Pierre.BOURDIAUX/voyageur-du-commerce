#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#ifndef __PARCOUS_OPTIMISATION_H__
#define __PARCOUS_OPTIMISATION_H__


double factorielle (int n);
double calcul_cycle_possible (int nmb_sommets);
void Crea_table_traitement (int * nmb_sommets,int taille_liste);
int element_present(int *L, int tailleL, int valeur) ;
int cout_glouton (int ** matrice,int nmb_sommets);

#endif