#include "./algo_fourmies.h"
#include "../graphes/creation_graphe.h"
#include "../Affichage/Gere_sommet_arete.h"
#include "../Affichage/Affiche_graphe.h"

int main() 
{
    int numNodes = 4;   // Nombre de noeuds du graphe
    int startNode = 0;  // Noeud de départ

    int ** matrice_distance;
    Graphe graphe;
    matrice_distance=creationMatrice (numNodes);
    graphe=genereGraphe(matrice_distance,numNodes,0.5);

    afficheMatrice(graphe,numNodes);
    printf("\n");

    Graphe graphe_complet = floyd_warshall(graphe,N);

    afficheMatrice (graphe_complet,numNodes);
    printf("\n");

    SDL_Rect *pointRect;
    pointRect = getCoordSommet();
    distanceAretes(graphe_complet,pointRect);

    afficheMatrice (graphe_complet,numNodes);
    printf("\n");
    
    Edge ** graph=NULL;
    graph=initialisation_graphe_edge(graphe_complet,numNodes);
    
    int* bestTour = (int*)malloc((2*numNodes+1) * sizeof(int));
    int bestLength;
    bestLength=antColonyOptimization(graph, numNodes, startNode, bestTour);
    
    // Affichage du meilleur tour trouvé
    printf("\nBest tour: ");
    for (int i = 0; i < numNodes; i++) {
        printf("%d ", bestTour[i]);
    }
    printf("\n Cout du meilleur Tour : %d\n",bestLength);
    
    free(bestTour);
    freeGraph(graph, numNodes);
    freeMatrice(graphe,numNodes);
    free (pointRect);
    
    return 0;
}