#include "./parcours_optimisation.h"
#include "../graphes/creation_graphe.h"
#include "./floyd_warshall.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>


int main(int argc, char **argv) 
{
    (void)argc;
    (void)argv;

    int nmb_sommets []= {5,10,15,20,30,40,50,60,70,80,90,100,1000};
    int taille_liste=13;
    Crea_table_traitement(nmb_sommets,taille_liste);

    int N = 10;
    int ** matrice_adjacence = creationMatrice(N);
    srand(time(NULL));
    matrice_adjacence = genereGraphe(matrice_adjacence,N, 0.5);
    printf("\n");
    afficheMatrice(matrice_adjacence,N);
    printf("\n");

    int ** matrice_distance = creationMatrice(N);
    matrice_distance= floyd_warshall (matrice_adjacence,N);

    afficheMatrice(matrice_distance,N);
    printf("\n");
    int cout =cout_glouton(matrice_distance,N);
    printf("%d\n",cout);

    freeMatrice(matrice_adjacence,N);
    freeMatrice(matrice_distance,N);



    



    return 0;
}
