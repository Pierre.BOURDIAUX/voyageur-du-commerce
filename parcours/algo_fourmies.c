#include "./algo_fourmies.h"

// Création du graphe avec la structure Edge 

Edge ** initialisation_graphe_edge (int ** graphe, int numNodes)
{
    Edge ** graph =  malloc(numNodes * sizeof(Edge*));
    for (int i = 0; i < numNodes; i++)
    {
        graph [i] = malloc(numNodes * sizeof(Edge));
        for (int j = 0; j < numNodes; j++) {
            graph[i][j].pheromone = 1.0;    // Initialisation de la phéromone
            graph[i][j].distance = graphe [i][j];   // Ajout de la distance
        }
    }
    return(graph);

}

// Fonction pour libérer la mémoire allouée pour le graphe
void freeGraph(Edge** graph, int numNodes) {
    int i;
    for (i = 0; i < numNodes; i++) {
        free(graph[i]);
    }
    free(graph);
}

// Fonction pour mettre à jour la phéromone évaporée sur toutes les arêtes
void evaporatePheromone(Edge** graph, int numNodes) {
    int i, j;
    for (i = 0; i < numNodes; i++) {
        for (j = 0; j < numNodes; j++) {
            graph[i][j].pheromone *= (1.0 - RHO);
        }
    }
}


// Fonction pour effectuer le mouvement d'une fourmi
int antMove(Edge** graph, int numNodes, int startNode, int* visited,int numEtape,int Start) {
    int i, nextNode;
    double total = 0.0;
    double* probabilities = (double*)malloc(numNodes * sizeof(double));
    int count=0;

    for (int j=0; j < numNodes; j++) 
    {
        if ( graph[startNode][j].distance!=0.0)
        {
            count ++;
        }
    }
    if (count>1)
    {
        for (i = 0; i < numNodes; i++) 
        {
            if (numEtape<numNodes)
            {
                if (!visited[i])
                {
                    // Calcul de la probabilité de choisir l'arête (startNode, i)
                    probabilities[i] = pow(graph[startNode][i].pheromone, ALPHA) * pow(1.0 / graph[startNode][i].distance, BETA);
                    total += probabilities[i];
                }
                else 
                {
                    probabilities[i] = 0.0;
                }
            }
            else
            {
                if ((graph[startNode][i].distance !=0.0)&&(i==Start))
                {
                    probabilities[i] = 1.0;
                }
                else 
                {
                    // Calcul de la probabilité de choisir l'arête (startNode, i)
                    probabilities[i] = pow(graph[startNode][i].pheromone, ALPHA) * pow(1.0 / graph[startNode][i].distance, BETA);
                    total += probabilities[i];
                }
            }
        }
        // Sélection de le prochain état à visiter selon les probabilités
        double random = (double)rand() / RAND_MAX;
        double p = 0.0;
        for (i = 0; i < numNodes; i++) 
        {
            if (numEtape<numNodes)
            {
                if (!visited[i]) 
                {
                    p += probabilities[i] / total;
                    if (random <= p) 
                    {
                        nextNode = i;
                        break;
                    }
                }

            }
            else
            {
                if (graph[startNode][i].distance !=0.0)
                {
                    nextNode = i;
                    break;
                }
                else
                {
                    p += probabilities[i] / total;
                    if (random <= p) 
                    {
                        nextNode = i;
                        break;
                    }
                }

            }
        }
    
    free(probabilities);
    }
    else
    {
        nextNode=startNode;
    }
    
    return nextNode;
    
}

// Fonction principale pour résoudre le problème en utilisant les colonies de fourmis
int antColonyOptimization(Edge** graph, int numNodes, int startNode, int* bestTour) {
    int i, j, k;
    int** antTours = (int**)malloc(NUM_ANTS * sizeof(int*));
    double* tourLengths = (double*)malloc(NUM_ANTS * sizeof(double));
    double bestLength = __DBL_MAX__;
    
    // Initialisation des tours de chaque fourmi
    for (i = 0; i < NUM_ANTS; i++) {
        antTours[i] = (int*)malloc((2*numNodes+1) * sizeof(int));
    }
    
    // Itérations de l'algorithme
    for (i = 0; i < MAX_ITERATIONS; i++) {
        // Déplacement de chaque fourmi
        for (j = 0; j < NUM_ANTS; j++) 
        {
            int currentNode = startNode;
            int* visited = (int*)malloc((2*numNodes+1) * sizeof(int));
            int numEtape=0;
            antTours[j][0] = currentNode;
            for (k = 0; k < numNodes; k++) 
            {
                visited[k] = 0;
            }
            visited[currentNode] = 1;
            numEtape++;
            int nextNode;
            int y=0;
            antTours[j][y] = currentNode;
            y++;

            //1ère étape
            nextNode=antMove(graph, numNodes, currentNode, visited,numEtape,startNode);
            antTours[j][y] = nextNode;
            if (!visited[nextNode])
            {
                visited[nextNode] = 1;
                numEtape++;

            }
            currentNode = nextNode;
            y++;
            
            
            // Construction du tour de la fourmi
            while (currentNode!=startNode)
            {
                nextNode = antMove(graph, numNodes, currentNode, visited,numEtape,startNode);
                antTours[j][y] = nextNode;
                if (!visited[nextNode])
                {
                    visited[nextNode] = 1;
                    numEtape++;
                }
                currentNode = nextNode;
                y++;
            }
            
            // Calcul de la longueur du tour de la fourmi
            double tourLength = 0.0;
            for (k = 0; k < numNodes - 1; k++) 
            {
                tourLength += graph[antTours[j][k]][antTours[j][k + 1]].distance;
            }
            
            tourLengths[j] = tourLength;
            if (tourLength < bestLength) 
            {
                bestLength = tourLength;
                memcpy(bestTour, antTours[j], numNodes * sizeof(int));
            }
            
            free(visited);
        }
        
        // Mise à jour de la phéromone évaporée sur toutes les arêtes
        evaporatePheromone(graph, numNodes);
        
        // Dépôt de la phéromone sur les arêtes du meilleur tour
        for (j = 0; j < numNodes - 1; j++) {
            int node1 = bestTour[j];
            int node2 = bestTour[j + 1];
            graph[node1][node2].pheromone += Q / bestLength;
        }
    }

        // Libération de la mémoire allouée
    for (i = 0; i < NUM_ANTS; i++) {
        free(antTours[i]);
    }
    free(antTours);
    free(tourLengths);
    return(bestLength);
}