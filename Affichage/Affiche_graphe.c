#include "./affiche_graphe.h"




SDL_Window* createWindows(char * titre,SDL_DisplayMode screen)
{
    SDL_Window* window = NULL;
    window = SDL_CreateWindow(titre,10, 10, screen.w * 0.95, screen.h * 0.9, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        printf("Erreur lors de la creation de la fenetre : %s", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    return window;
}

SDL_Renderer *createRenderer(SDL_Window* window)
{
    SDL_Renderer* renderer = NULL;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
    {
        printf("Erreur lors de la creation du renderer : %s", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    return renderer;
}

void afficheGraphe(Graphe graphe){


    for (int i = 0; i < N; i++)
    {
        for (int j = i; j < N; j++)
        {
           printf("%d ", graphe[i][j]);
        }
        printf("\n");
    }

    if (SDL_Init(SDL_INIT_VIDEO) != 0) SDL_Quit();                                


    SDL_DisplayMode screen;
    SDL_GetCurrentDisplayMode(0, &screen);
    //printf("screen.w : %d, screen.h : %d\n",screen.w,screen.h);

    SDL_Window* window = createWindows("Graphe",screen );
    SDL_Renderer* renderer = createRenderer(window);

    SDL_Rect *rectPoint;
    int window_w, window_h;    
    int offset_x, offset_y;

    SDL_GetWindowSize(window, &window_w, &window_h);
    //printf("window_w : %d, window_h : %d\n",window_w,window_h);

    offset_x = window_w / (N*3);
    offset_y = window_h / (N*3);



    rectPoint = getCoordSommet();
    rectPoint=CoordMatriceToCoordScreen(rectPoint,offset_x,offset_y);

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    for (int i = 0; i < N; i++)
    {
        SDL_RenderFillRect(renderer, &rectPoint[i]);                        

    }   
    creerAretes(graphe,renderer,rectPoint);
    distanceAretes(graphe,rectPoint); // calcul de la distance entre les sommets et stockage dans le graphe
    
    SDL_RenderPresent(renderer);

    GestionEvenement(window, renderer, rectPoint, graphe);                     
    
}


void GestionEvenement(SDL_Window* window, SDL_Renderer *renderer, SDL_Rect* rectSommet, Graphe graphe){
    int x, y;
    int numSommet =-1;
    int ancienSommet = -1;
    int nbSommetVisite = 0;
    int poidsTotal = 0;
    int premierSommet = -1;
    SDL_bool
      program_on = SDL_TRUE,                         
      paused = SDL_FALSE,                           
      event_utile = SDL_FALSE;                      
    SDL_Event event;                                 

    while (program_on) {                            
      event_utile = SDL_FALSE;
      while(!event_utile && SDL_PollEvent(&event)) {  
        
            switch (event.type) {                         
                case SDL_QUIT:                              
                    program_on = SDL_FALSE;                   
                    event_utile = SDL_TRUE;
                    break;
                
                
                case SDL_MOUSEBUTTONDOWN:                     // Click souris   
                    if (SDL_GetMouseState(&x, &y) & 
                        SDL_BUTTON(SDL_BUTTON_LEFT) ) {         // Si c'est un click gauche
                                printf("X: %d Y: %d \n", x,y);
                                numSommet=verifClickSommet(rectSommet, x, y);
                                printf("numSommet : %d\n",numSommet);
                                printf("ancienSommet : %d\n",ancienSommet);
                                if(ancienSommet == -1 && numSommet != -1){
                                    visiteSommet(numSommet,renderer,rectSommet);
                                    ancienSommet = numSommet;
                                    premierSommet = numSommet;
                                    if(graphe[numSommet][numSommet]==0){
                                        nbSommetVisite++;
                                        graphe[numSommet][numSommet]=1;
                                    }
                                }
                                else{
                                    if(numSommet != -1){
                                        if(graphe[ancienSommet][numSommet]>0){
                                            printf("graphe[%d][%d] : %d\n",ancienSommet,numSommet,graphe[ancienSommet][numSommet]);
                                            poidsTotal+=graphe[ancienSommet][numSommet];
                                            visiteSommet(numSommet,renderer,rectSommet);
                                            visiteArrete(ancienSommet,numSommet,renderer,rectSommet);
                                            ancienSommet = numSommet;
                                            if(graphe[numSommet][numSommet]==0){
                                                nbSommetVisite++;
                                                graphe[numSommet][numSommet]=1;
                                            }   
                                        }
                                    }
                                }
                                if(nbSommetVisite == N && numSommet == premierSommet){
                                    program_on = SDL_FALSE;
                                }
                            
                                event_utile = SDL_TRUE;

                        }       
                     break;
                default:                                  
                    break;
            }
        }  
    }

    SDL_DisplayMode screen;
    SDL_GetCurrentDisplayMode(0, &screen);

    int window_w, window_h;    

    SDL_GetWindowSize(window, &window_w, &window_h);

    Graphe graphe_coord_exact = floyd_warshall(graphe,N);

    SDL_Rect *pointRect;
    pointRect = getCoordSommet();
    pointRect= CoordMatriceToCoordScreen(pointRect,window_w / (N*3),window_h / (N*3));
    distanceAretes(graphe_coord_exact,pointRect);
    afficheMatrice(graphe_coord_exact,N);


    etat * state = simulatedAnnealing(graphe_coord_exact,N,100000,calculPoids);
    int bestLength = state->poids;

    printf("poidsTotal : %d\n",poidsTotal); 
    printf("poidsTotal : %d\n",bestLength);   

    int difference =(poidsTotal - bestLength-250 ); // pas aussi précis que la machine
    affichage_score(poidsTotal,difference);
    

}

void visiteSommet(int numSommet, SDL_Renderer *renderer, SDL_Rect* rectSommet){
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
    SDL_RenderFillRect(renderer, &rectSommet[numSommet]);
    SDL_RenderPresent(renderer);
}

void visiteArrete(int sommet1, int sommet2, SDL_Renderer *renderer, SDL_Rect* rectSommet){
    printf("sommet1 : %d, sommet2 : %d\n",sommet1,sommet2);
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 10);
    SDL_RenderDrawLine(renderer, rectSommet[sommet1].x+rectSommet[sommet1].w/2, rectSommet[sommet1].y+rectSommet[sommet1].h/2, rectSommet[sommet2].x+rectSommet[sommet2].w/2, rectSommet[sommet2].y+rectSommet[sommet2].h/2);
    SDL_RenderPresent(renderer);
}



void creerAretes(Graphe graph, SDL_Renderer* renderer, SDL_Rect* rectPoint){

    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 10);

    for (int i = 0; i < N; i++)
    {
        for (int j = i; j < N; j++)
        {
            printf(" %d",graph[i][j]);
            if(graph[i][j]==1){
                SDL_RenderDrawLine(renderer, rectPoint[i].x+rectPoint[i].w/2,
                 rectPoint[i].y+rectPoint[i].h/2, 
                 rectPoint[j].x+rectPoint[j].w/2, 
                 rectPoint[j].y+rectPoint[j].h/2);
            }
        }
        printf("\n");
    }
    
    SDL_RenderPresent(renderer);                     

    
}


void affichage_score(int PoidsJoueur, int difference) 
{
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    SDL_Window* window = NULL;
    window = SDL_CreateWindow("Score",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, 800, 800, SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    TTF_Font* font = TTF_OpenFont("./RobotoSlab-Medium.ttf", 24);
    if (font == NULL) {
        printf("Erreur lors du chargement de la police : %s\n", TTF_GetError());
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Color textColor = { 255, 255, 255 };

    // Texte à afficher
    char text[100];
    sprintf(text, "Poids Joueur : %d ", PoidsJoueur);
    char * text2 = " Bravo vous avez gagne(avec accent mais marche pas) ";
    char text3[100];
    sprintf(text3, "Vous avez perdu, difference de % d ", difference);

    SDL_Surface* surface = TTF_RenderText_Solid(font, text, textColor);
    SDL_Surface* surface2 ;

    if (difference > 0) 
    {
        surface2 = TTF_RenderText_Solid(font, text3, textColor);
    }
    else 
    {
       surface2 = TTF_RenderText_Solid(font, text2, textColor);
    }

    if (surface == NULL) 
    {
        printf("Erreur lors de la création de la surface : %s\n", TTF_GetError());
        TTF_CloseFont(font);
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    if (surface2 == NULL) 
    {
        printf("Erreur lors de la création de la surface : %s\n", TTF_GetError());
        TTF_CloseFont(font);
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_Texture* texture2 = SDL_CreateTextureFromSurface(renderer, surface2);
    if (texture == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        SDL_FreeSurface(surface);
        TTF_CloseFont(font);
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    if (texture2 == NULL) {
        printf("Erreur lors de la création de la texture : %s\n", SDL_GetError());
        SDL_FreeSurface(surface);
        TTF_CloseFont(font);
        TTF_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_FreeSurface(surface);
    SDL_FreeSurface(surface2);
    int textureWidth, textureHeight,textureWidth2, textureHeight2;
    SDL_QueryTexture(texture, NULL, NULL, &textureWidth, &textureHeight);
    SDL_QueryTexture(texture2, NULL, NULL, &textureWidth2, &textureHeight2);

    int textX = (800 - textureWidth) / 2;
    int textY = (800 - textureHeight) / 2;
    int textX2 = (900 - textureWidth2) / 2;
    int textY2 = (900 - textureHeight2) / 2;

    SDL_RenderClear(renderer);

    SDL_Rect destRect = { textX, textY, textureWidth, textureHeight };
    SDL_Rect destRect2 = { textX2, textY2, textureWidth2, textureHeight2 };
    SDL_RenderCopy(renderer, texture, NULL, &destRect);
    SDL_RenderCopy(renderer, texture2, NULL, &destRect2);

    SDL_RenderPresent(renderer);
    SDL_Delay (5000);

    SDL_DestroyTexture(texture);
    SDL_DestroyTexture(texture2);
    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
}