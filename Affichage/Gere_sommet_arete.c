#include "./Gere_sommet_arete.h"


void distanceAretes(Graphe graphe, SDL_Rect *pointRect){

    for (int i = 0; i < N; i++)
    {
        for (int j = i; j < N; j++)
        {
           if(graphe[i][j]!=0){
            graphe[i][j]=sqrt(pow(pointRect[i].x-pointRect[j].x,2)+pow(pointRect[i].y-pointRect[j].y,2));
            graphe[j][i]=graphe[i][j];
           }
        }
    }
}


int verifClickSommet(SDL_Rect * rectSommet,int x,int y){
    int numSommet = -1;
    for (int i = 0; i < N; i++)
    {
        if(x>rectSommet[i].x && x<rectSommet[i].x+rectSommet[i].w && y>rectSommet[i].y && y<rectSommet[i].y+rectSommet[i].h){
            numSommet = i;
            break;
        }
    }
    return numSommet;
}

SDL_Rect* getCoordSommet(){
    SDL_Rect* coordSommet = malloc(N*sizeof(SDL_Rect));
    int nbChangement = 0;
    int tabCase[N*3][N*3];
    for (int i = 0; i < N*3; i++)
    {
        for (int j = 0; j < N*3; j++)
        {
            tabCase[i][j]=0;
        }
    }

    int random;
    for(int i=0;i<N;i++){
        random = rand()% ((3*N * 3*N)-nbChangement); // on choisit une case au hasard
        for(int j=0;j<N*3;j++){
            for(int k=0;k<N*3;k++){
                if(tabCase[j][k]==0){
                    random--;
                }
                if(random==0){
                   nbChangement+= videtoNonVide(tabCase,j,k); // on met la case en vide
                   coordSommet[i].x = j;
                   coordSommet[i].y = k;
                   coordSommet[i].w = 30;
                   coordSommet[i].h = 30;
                   j = k = N*3; // on sort des boucles


                }
            }
        }
    }

        return coordSommet;
}

SDL_Rect* CoordMatriceToCoordScreen(SDL_Rect * coordMatrice, int offset_x, int offset_y){
    for(int i=0;i<N;i++){
        coordMatrice[i].x = ((rand()%100)*0.01) * offset_x + offset_x*coordMatrice[i].x;
        coordMatrice[i].y =((rand()%100)*0.01) * offset_y + offset_y*coordMatrice[i].y;

    }
    return coordMatrice;
}



int videtoNonVide(int tabCase[][N*3], int x, int y){
    int nbChangement=1; //la case est forcément libre au début
    tabCase[x][y]=2;

    if(x>0){
        if(tabCase[x-1][y]==0){
            nbChangement++;
            tabCase[x-1][y]=1;
        }
    }
    if(x<N*3-1){
         if(tabCase[x+1][y]==0){
            nbChangement++;
            tabCase[x+1][y]=1;
        }
    }
    if(y>0){
        if(tabCase[x][y-1]==0){
            nbChangement++;
            tabCase[x][y-1]=1;
        }

    }
    if(y<N*3-1){
        if(tabCase[x][y+1]==0){
            nbChangement++;
            tabCase[x][y+1]=1;
        }

    }
    if(x>0 && y>0){
        if(tabCase[x-1][y-1]==0){
            nbChangement++;
            tabCase[x-1][y-1]=1;
        }
    }
    if(x>0 && y<N*3-1){
        if(tabCase[x-1][y+1]==0){
            nbChangement++;
            tabCase[x-1][y+1]=1;
        }
    }
    if(x<N*3-1 && y>0){
        if(tabCase[x+1][y-1]==0){
            nbChangement++;
            tabCase[x+1][y-1]=1;
        }
    }
    if(x<N*3-1 && y<N*3-1){
        if(tabCase[x+1][y+1]==0){
            nbChangement++;
            tabCase[x+1][y+1]=1;
        }
    }
    return nbChangement;

}