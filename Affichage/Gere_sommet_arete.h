#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <SDL2/SDL.h>

#define N 4

typedef int** Graphe;


void distanceAretes(Graphe graphe, SDL_Rect *pointRect);

int verifClickSommet(SDL_Rect * rectSommet,int x,int y);

SDL_Rect* CoordMatriceToCoordScreen(SDL_Rect * coordMatrice, int offset_x, int offset_y);

SDL_Rect* getCoordSommet();

int videtoNonVide(int tabCase[][N*3], int x, int y);