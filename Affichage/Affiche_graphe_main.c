#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "../graphes/creation_graphe.h"
#include "./affiche_graphe.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


int main(int argc, char const *argv[])
{
    (void)argc;
    (void)argv;

    Graphe matrice = creationMatrice(N);
    srand(time(NULL));
    matrice = genere(matrice, N,0, N - 1);
    matrice = genereGraphe(matrice,N, 0.5);


    afficheGraphe(matrice);

    return 0;
}
