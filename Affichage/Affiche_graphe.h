#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <SDL2/SDL.h>
 #include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>
#include "Gere_sommet_arete.h"
#include "../graphes/creation_graphe.h"
#include "../parcours/floyd_warshall.h"
#include "../recuit_simulé.h"




typedef int** Graphe;
// typedef struct arete{
//     int sommet1;
//     int sommet2;
//     int norme;
//     struct arete * suivant;
// }Arete;

#define N 4

SDL_Window* createWindows(char * titre,SDL_DisplayMode screen);
SDL_Renderer* createRenderer(SDL_Window* window);
void afficheGraphe(Graphe graphe);
void creerAretes(Graphe graph, SDL_Renderer* renderer, SDL_Rect* rectPoint);
void GestionEvenement(SDL_Window* window, SDL_Renderer* renderer, SDL_Rect* rectPoint, Graphe graphe);
void visiteSommet(int numSommet, SDL_Renderer *renderer, SDL_Rect* rectSommet);
void visiteArrete(int sommet1, int sommet2, SDL_Renderer *renderer, SDL_Rect* rectSommet);
void affichage_score (int PoidsJoueur,int difference);
